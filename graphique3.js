var evtAfficher = document.querySelector('#boutonAfficherHistogramme');
evtAfficher.addEventListener('click', drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Navigateur', 'Parts de marché en %'],
        ['IE + Edge',     7],
        ['Safari',      19],
        ['Firefox',  10],
        ['Chrome', 55],
        ['Opera',    3],
        ['Autres',    6],
    ]);

    var options = {
        title: 'My Daily Activities',
        pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
}