var evtAfficher = document.querySelector('#boutonAfficherHistogramme');
evtAfficher.addEventListener('click', drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ['Navigateur', 'Parts de marché en %'],
        ['IE + Edge',7],
        ['Safari',19],
        ['Firefox',10],
        ['Chrome',55],
        ['Opera',3],
        ['Autres',6],
    ]);

    var options = {
        width: 1000, height: 300,
        redFrom: 90, redTo: 100,
        yellowFrom:75, yellowTo: 90,
        minorTicks: 5
    };

    var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

    chart.draw(data, options);

    setInterval(function() {
        data.setValue(0, 1);
        chart.draw(data, options);
    }, 0);
    setInterval(function() {
        data.setValue(1, 1);
        chart.draw(data, options);
    }, 0);
    setInterval(function() {
        data.setValue(2, 1);
        chart.draw(data, options);
    }, 0);
    setInterval(function() {
        data.setValue(3, 1);
        chart.draw(data, options);
    }, 0);
    setInterval(function() {
        data.setValue(4, 1);
        chart.draw(data, options);
    }, 0);
    setInterval(function() {
        data.setValue(5, 1);
        chart.draw(data, options);
    }, 0);
}