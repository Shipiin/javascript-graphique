var evtAfficher = document.querySelector('#boutonAfficherHistogramme');
evtAfficher.addEventListener('click', tracerHistogramme);

/* Fonctionne tracerGraphe */
function tracerHistogramme() {

    /* Données à representer graphiquement */
    var donnees = google.visualization.arrayToDataTable([
        ['Année','Allemagne','France','Italie','Moyenne'],
        ['2010',100,110,150,120],
        ['2011',120,130,200,150],
        ['2012',140,120,220,160],
    ]);

    var options = {
        title: 'Production annuelle du produit LAMBDA par pays',
        vAxis: {title: "Tonnes" },
        hAxis: {title: "Années" },
        seriesType: "bars",
        series: {3:{type:"line"}}
    };

    /* Instanciation du graphique */
    let chart = new google.visualization.ComboChart(document.getElementById('chart_Histogramme'));

    /* Dessin du graphique */
    chart.draw(donnees, options);
}