var evtAfficher = document.querySelector('#boutonAfficherHistogramme');
evtAfficher.addEventListener('click', drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ['Navigateur', 'Parts de marché en %'],
        ['IE + Edge',     7],
        ['Safari',      19],
        ['Firefox',  10],
        ['Chrome', 55],
        ['Opera',    3],
        ['Autres',    6],
    ]);

    var options = {
        title: 'Parts de marchee des principaux navigateurs web'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}

